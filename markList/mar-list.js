
const toggleButton = document.getElementById('ninety-two');
const flexContainer = document.querySelector('.year-list-filter-selected');

function toggleElements() {
  if (flexContainer.style.display === 'none' || flexContainer.style.display === '') {
    flexContainer.style.display = 'flex'; 
    toggleButton.style.background = 'darkgreen';
    toggleButton.style.color = '#fff';
  } else {
    flexContainer.style.display = 'none'; 
    toggleButton.style.background = 'aquamarine';
    toggleButton.style.color = 'black';
  }
}

toggleButton.addEventListener('click', toggleElements);